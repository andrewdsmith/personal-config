#!/bin/bash

install_linked_directory () {
  from=$1
  to=$2

  echo "Linking from $from to $to"

  # Only overwrite the symlinks not actual directories.
  if [ -d $from ] && [ ! -L $from ]
  then
    echo "ERROR: $from already exists and is not a symlink"
    exit
  fi

  # Using --no-derefernece makes the linking idempotent.
  ln --symbolic --no-dereference --force $to $from
}

install_linked_directory $HOME/.config/git $PWD/git
install_linked_directory $HOME/.vim $PWD/vim

if [ ! -e vim/autoload/plug.vim ]
then
  echo "Downloading vim-plug"

  sudo apt install curl

  # See https://github.com/junegunn/vim-plug#installation
  curl -fLso vim/autoload/plug.vim --create-dirs \
    https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
fi
